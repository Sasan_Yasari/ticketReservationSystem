package common;

public class SeatClass {
    private char name;

    public char getCapacity() {
        return capacity;
    }

    public void setCapacity(char capacity) {
        this.capacity = capacity;
    }

    char capacity;
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public int getAdultPrice() {
        return adultPrice;
    }

    public void setAdultPrice(int adultPrice) {
        this.adultPrice = adultPrice;
    }

    public int getChildPrice() {
        return childPrice;
    }

    public void setChildPrice(int childPrice) {
        this.childPrice = childPrice;
    }

    public int getInfantPrice() {
        return infantPrice;
    }

    public void setInfantPrice(int infantPrice) {
        this.infantPrice = infantPrice;
    }

    private String source, destination, airlineCode;
    private int adultPrice, childPrice, infantPrice;

    public char getName() {
        return name;
    }

    public void setName(char name) {
        this.name = name;
    }

    public SeatClass(String nameCap, String source, String destination, String airlineCode,
                     int adultPrice, int childPrice, int infantPrice) {
        this.setName(nameCap.charAt(0));
        this.setCapacity(nameCap.charAt(1));
        this.setSource(source);
        this.setDestination(destination);
        this.setAirlineCode(airlineCode);
        this.setAdultPrice(adultPrice);
        this.setChildPrice(childPrice);
        this.setInfantPrice(infantPrice);
    }

    public int getPrice(int adultCount, int childCount, int infantCount) {
        return  adultCount * adultPrice
                + childCount * childPrice
                + infantCount * infantPrice;
    }

}
