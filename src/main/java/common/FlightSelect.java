package common;

public class FlightSelect {
    public String getKey() {
        return key;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    private int price;

    public void setKey(String key) {
        this.key = key;
    }

    String key;


    public char getSeat() {
        return seat;
    }

    public void setSeat(char seat) {
        this.seat = seat;
    }

    char seat;

}
