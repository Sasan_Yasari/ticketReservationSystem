package common;

import java.util.Arrays;
import java.util.List;

public class Flight {
    private String source, destination, airlineCode, date, departure, arrival, airplaneModel, key;
    private int flightNumber;
    private SeatClass seatClass;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public SeatClass getSeatClass() {
        return seatClass;
    }

    public void setSeatClass(SeatClass seatClass) {
        this.seatClass = seatClass;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public String getAirplaneModel() {
        return airplaneModel;
    }

    public void setAirplaneModel(String airplaneModel) {
        this.airplaneModel = airplaneModel;
    }

    public int getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(int flightNumber) {
        this.flightNumber = flightNumber;
    }

    public Flight(String info, SeatClass sc) {
        List<String> infoSplit = Arrays.asList(info.split("\\s+"));
        this.setAirlineCode(infoSplit.get(0));
        this.setFlightNumber(Integer.parseInt(infoSplit.get(1)));
        this.setDate(infoSplit.get(2));
        this.setSource(infoSplit.get(3));
        this.setDestination(infoSplit.get(4));
        this.setDeparture(infoSplit.get(5).substring(0, 2) + ":" + infoSplit.get(5).substring(2));
        this.setArrival(infoSplit.get(6).substring(0, 2) + ":" + infoSplit.get(6).substring(2));
        this.setAirplaneModel(infoSplit.get(7));
        this.setSeatClass(sc);
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    private int totalPrice;

    public int getPrevPrice() {
        return prevPrice;
    }

    public void setPrevPrice(int prevPrice) {
        this.prevPrice = prevPrice;
    }

    private int prevPrice;

    public int getPrice(int adultCount, int childCount, int infantCount) {
        this.totalPrice = this.seatClass.getPrice(adultCount, childCount, infantCount);
        return this.totalPrice;
    }
    public Flight() {
    }

    public int getAdultPrice() {
        return seatClass.getAdultPrice();
    }

    public int getChildPrice() {
        return seatClass.getChildPrice();
    }

    public int getInfantPrice() {
        return seatClass.getInfantPrice();
    }
    public String getPersianSource() {
        if(this.getSource().equals("THR"))
            return "تهران";
        else if (getSource().equals("MHD")) {
            return "مشهد";
        }
        else if (getSource().equals("IFN")) {
            return "اصفهان";
        }
        else return "";
    }
    public String getPersianDestination() {
        if(this.getDestination().equals("THR"))
            return "تهران";
        else if (getDestination().equals("MHD")) {
            return "مشهد";
        }
        else if (getDestination().equals("IFN")) {
            return "اصفهان";
        }
        return "";
    }

    public String getAirlineCodePersian() {
        if (this.getAirlineCode().equals("IR"))
            return "ایران ایر";
        else if (this.getAirlineCode().equals("W5"))
            return "ماهان";
        return ";";
    }

}
