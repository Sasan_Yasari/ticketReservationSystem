package common;

import serverInterface.Provider;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Repository {
    public static final String CONN_STR = "jdbc:hsqldb:hsql://localhost/xdb";

    private static Repository repository = new Repository();

    private Repository() {
    }

    private int adultCount;
    private int childCount;
    private int infantCount;


    static {
        try {
            Class.forName("org.hsqldb.jdbcDriver");
        } catch (ClassNotFoundException ex) {
            System.err.println("Unable to load HSQLDB JDBC driver");
        }
    }

    private void getFlightFromProvider(String source, String destination, String date) throws SQLException, IOException {
        Connection connection = DriverManager.getConnection(CONN_STR, "SA", "");
        Statement statement = connection.createStatement();
        statement.executeQuery("update flights set valid=false");
        connection.close();

        Provider provider = new Provider();
        String response = provider.search(source, destination, date);
        List<String> responseSplit = Arrays.asList(response.split("\\r?\\n"));
        int responses = provider.getLine()/2 + 1;
        for (int j = 0; j < responses; j += 2) {
            Flight flight = new Flight();
            String key = keyGenerator();
            List<String> seats = Arrays.asList(responseSplit.get(j + 1).split("\\s+"));
            for(int k = 0; k < seats.size(); k++) {
                if (seats.get(k).charAt(1) != 'C') {
                    List<String> resSplit = Arrays.asList(responseSplit.get(j).split("\\s+"));
                    String prices = provider.price(source, destination, resSplit.get(0), seats.get(k).charAt(0));
                    List<String> pricesSplit = Arrays.asList(prices.split("\\s+"));
                    SeatClass sc = new SeatClass(seats.get(k), source, destination,
                            resSplit.get(0),
                            Integer.parseInt(pricesSplit.get(0)),
                            Integer.parseInt(pricesSplit.get(1)),
                            Integer.parseInt(pricesSplit.get(2)));
                    flight = new Flight(responseSplit.get(j), sc);
                    flight.setKey(key);
                    Repository.getRepository().insertSeat(sc, key);
                }
            }
            Repository.getRepository().insertFlight(flight);
        }
    }

    private void insertSeat(SeatClass seatClass, String flightIndex) throws SQLException {
        Connection connection = DriverManager.getConnection(CONN_STR, "SA", "");
        Statement statement = connection.createStatement();
        statement.executeQuery("insert into SEATCLASSES values " +
                " ('" + seatClass.getName() + "', '" + seatClass.getCapacity() + "', '" + seatClass.getSource() + "', " +
                "'" + seatClass.getDestination() + "', '" + seatClass.getAirlineCode() + "', " + seatClass.getAdultPrice() + ", " +
                + seatClass.getChildPrice() + ", " + seatClass.getInfantPrice() + ", '" + flightIndex + "') " );
        connection.close();
    }

    private void insertFlight(Flight flight) throws SQLException {
        Connection connection = DriverManager.getConnection(CONN_STR, "SA", "");
        Statement statement = connection.createStatement();
        statement.executeQuery("insert into flights values" +
                " ('" + flight.getKey() + "', '" + flight.getSource() + "', '" + flight.getDestination() +
                "', '" + flight.getDate() + "', '" + flight.getAirlineCode() + "', '" + flight.getDeparture() +
                "', '" + flight.getArrival() + "', '" + flight.getAirplaneModel() + "', " + flight.getFlightNumber() +
                ", " + System.currentTimeMillis() + " , true) " );
        connection.close();
    }

    public ArrayList<Flight> getFlights(String source, String destination, String date) throws SQLException, IOException {

        Connection connection = DriverManager.getConnection(CONN_STR, "SA", "");
        Statement statement = connection.createStatement();

        ResultSet flightResultSet = statement.executeQuery("select * from FLIGHTS where source='" + source + "' and " +
                "destination='" + destination + "' and date='" + date + "' and valid='" + true + "'");

        if(!flightResultSet.next()) {
            getFlightFromProvider(source, destination, date);
            flightResultSet = statement.executeQuery("select * from FLIGHTS where source='" + source + "' and " +
                    "destination='" + destination + "' and date='" + date +"' and valid='" + true + "'");
        }
        else if(System.currentTimeMillis() - flightResultSet.getLong("time") > 1800000) {
            getFlightFromProvider(source, destination, date);
            flightResultSet = statement.executeQuery("select * from FLIGHTS where source='" + source + "' and " +
                    "destination='" + destination + "' and date='" + date +"' and valid='" + true + "'");
        }
        else{
            flightResultSet = statement.executeQuery("select * from FLIGHTS where source='" + source + "' and " +
                    "destination='" + destination + "' and date='" + date +"' and valid='" + true + "'");
        }
        ArrayList<Flight> flights = new ArrayList<>();

        while(flightResultSet.next()) {
            String flightInfo = flightResultSet.getString(5) + " " +
                    flightResultSet.getString(9) + " " +
                    flightResultSet.getString(4) + " " +
                    flightResultSet.getString(2) + " " +
                    flightResultSet.getString(3) + " " +
                    flightResultSet.getString(6).substring(0, 2) + flightResultSet.getString(6).substring(3) + " " +
                    flightResultSet.getString(7).substring(0, 2) + flightResultSet.getString(7).substring(3) + " " +
                    flightResultSet.getString(8) + " ";

            ResultSet seatClassResultSet = statement.executeQuery("select * from seatclasses where source='" + source + "' and " +
                    "destination='" + destination + "' and airlineCode='" + flightResultSet.getString(5) +
                    "' and flightIndex='" + flightResultSet.getString("key") + "'");

            while(seatClassResultSet.next()) {
                SeatClass seatClass = new SeatClass(seatClassResultSet.getString(1) + seatClassResultSet.getString(2),
                        seatClassResultSet.getString(3), seatClassResultSet.getString(4), seatClassResultSet.getString(5),
                        seatClassResultSet.getInt(6), seatClassResultSet.getInt(7), seatClassResultSet.getInt(8));

                Flight flight = new Flight(flightInfo, seatClass);
                flight.setKey(flightResultSet.getString(1));
                flights.add(flight);
            }
            seatClassResultSet.close();
        }
        flightResultSet.close();

        connection.close();
        return flights;
    }

    public Flight getFlight(String key, char seat) throws SQLException {
        Connection connection = DriverManager.getConnection(CONN_STR, "SA", "");
        Statement statement = connection.createStatement();
        ResultSet flightResultSet = statement.executeQuery("select * from FLIGHTS where key='" + key + "'");
        flightResultSet.next();
        String flightInfo = flightResultSet.getString(5) + " " +
                flightResultSet.getString(9) + " " +
                flightResultSet.getString(4) + " " +
                flightResultSet.getString(2) + " " +
                flightResultSet.getString(3) + " " +
                flightResultSet.getString(6).substring(0, 2) + flightResultSet.getString(6).substring(3) + " " +
                flightResultSet.getString(7).substring(0, 2) + flightResultSet.getString(7).substring(3) + " " +
                flightResultSet.getString(8) + " ";
        ResultSet seatClassResultSet = statement.executeQuery("select * from seatclasses where name='" + seat + "'");
        seatClassResultSet.next();
        SeatClass seatClass = new SeatClass(seatClassResultSet.getString(1) + seatClassResultSet.getString(2),
                seatClassResultSet.getString(3), seatClassResultSet.getString(4), seatClassResultSet.getString(5),
                seatClassResultSet.getInt(6), seatClassResultSet.getInt(7), seatClassResultSet.getInt(8));
        connection.close();
        Flight flight = new Flight(flightInfo, seatClass);
        flight.setKey(flightResultSet.getString("key"));
        return flight;
    }

    private void updateSeatPrices(char name, String key, int adultPrice, int childPrice, int infantPrice) throws SQLException {
        Connection connection = DriverManager.getConnection(CONN_STR, "SA", "");
        Statement statement = connection.createStatement();
        statement.executeQuery("update SEATCLASSES set adultPrice=" + adultPrice +
                ", childPrice=" + childPrice + ", infantPrice=" + infantPrice +
                " where flightIndex='" + key + "' and name='" + name + "'");
        connection.close();
    }

    public List<String> updatePrices(Flight flight, FlightSelect flightSelect) throws IOException, SQLException {
        Provider provider = new Provider();
        String prices = provider.price(flight.getSource(), flight.getDestination(), flight.getAirlineCode(), flightSelect.getSeat());
        List<String> pricesSplit = Arrays.asList(prices.split("\\s+"));

        Repository.getRepository().updateSeatPrices(flightSelect.getSeat(),
                flightSelect.getKey(),
                Integer.parseInt(pricesSplit.get(0)),
                Integer.parseInt(pricesSplit.get(1)),
                Integer.parseInt(pricesSplit.get(2)));
        return pricesSplit;
    }

    public int getAdultCount() {
        return adultCount;
    }

    public void setAdultCount(int adultCount) {
        this.adultCount = adultCount;
    }

    public int getChildCount() {
        return childCount;
    }

    public void setChildCount(int childCount) {
        this.childCount = childCount;
    }

    public int getInfantCount() {
        return infantCount;
    }

    public void setInfantCount(int infantCount) {
        this.infantCount = infantCount;
    }

    private String keyGenerator() {
        Random random = new Random();
        String key ="";
        for(int i = 0; i < 10; i++)
            key += (char) (65+random.nextInt(57));
        return key;
    }

    public static Repository getRepository() {
        return repository;
    }

}
