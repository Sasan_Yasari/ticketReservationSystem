package common;

/**
 * Created by sasan on 5/23/17.
 */
public class Ticket {
    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    private Passenger passenger;
    private Flight flight;

    public Ticket(Flight flight, Passenger passenger) {
        this.passenger = passenger;
        this.flight = flight;
    }
}
