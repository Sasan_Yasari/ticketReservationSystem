package common;

import java.sql.*;

public class PassengerRepository {

    public static final String CONN_STR = "jdbc:hsqldb:hsql://localhost/xdb";

    private static PassengerRepository repository = new PassengerRepository();

    private PassengerRepository() {
    }

    public static PassengerRepository getRepository() {
        return repository;
    }

    public boolean getPassengerInfo(LoginInfo loginInfo) throws SQLException {
        Connection connection = DriverManager.getConnection(CONN_STR, "SA", "");
        PreparedStatement preparedStatement = connection.prepareStatement("select * from users where username=? and password=?");
        preparedStatement.setString(1, loginInfo.getUsername());
        preparedStatement.setString(2, loginInfo.getPassword());
        ResultSet resultSet = preparedStatement.executeQuery();
        connection.close();
        if (!resultSet.next())
            return false;
        return true;
    }

    public boolean addAccount(LoginInfo loginInfo, String sessionId) throws SQLException {
        Connection connection = DriverManager.getConnection(CONN_STR, "SA", "");
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from " +
                "users where username='" + loginInfo.getUsername() + "'");
        connection.close();
        if(!resultSet.next()){
            connection = DriverManager.getConnection(CONN_STR, "SA", "");
            statement = connection.createStatement();
            statement.executeQuery("insert into users values ('" + loginInfo.getUsername() +
                    "', '" + loginInfo.getPassword() +"', '" + loginInfo.getEmail() + "', '"
                    + sessionId + "', '');");
            connection.close();
            return true;
        }
        return false;
    }

    public void addSessionID(String username, String id) throws SQLException {
        Connection connection = DriverManager.getConnection(CONN_STR, "SA", "");
        Statement statement = connection.createStatement();
        statement.executeQuery("update users set sessionid='"
                + id + "' where username='" + username + "'");
        connection.close();
    }

    public String getSessionId(String username) throws SQLException {
        Connection connection = DriverManager.getConnection(CONN_STR, "SA", "");
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select sessionid from users " +
                        "where username='" + username + "'");
        connection.close();
        if(!resultSet.next())
            return "";
        return resultSet.getString("sessionid");
    }

    public void logout(String username) throws SQLException {
        Connection connection = DriverManager.getConnection(CONN_STR, "SA", "");
        Statement statement = connection.createStatement();
        statement.executeQuery("update users set sessionid='"
                + null + "' where username='" + username + "'");
        connection.close();
    }

    public void addReserveKey(String key, String username) throws SQLException {
        if(username == null || username == "admin")
            return;
        Connection connection = DriverManager.getConnection(CONN_STR, "SA", "");
        Statement statement = connection.createStatement();
        statement.executeQuery("update users set reservekey='"
                + key + "' where username='" + username + "'");
        connection.close();
    }

}
