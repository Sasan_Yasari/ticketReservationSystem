package common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Reserve {
    private String referenceCode, token;
    private int adultCount;
    private int childCount;
    private int infantCount;

    public int getAdultPrice() {
        return adultPrice;
    }

    public void setAdultPrice(int adultPrice) {
        this.adultPrice = adultPrice;
    }

    public int getChildPrice() {
        return childPrice;
    }

    public void setChildPrice(int childPrice) {
        this.childPrice = childPrice;
    }

    public int getInfantPrice() {
        return infantPrice;
    }

    public void setInfantPrice(int infantPrice) {
        this.infantPrice = infantPrice;
    }

    public void setTicketNumbers(ArrayList<String> ticketNumbers) {
        this.ticketNumbers = ticketNumbers;
    }

    private int adultPrice;
    private int childPrice;
    private int infantPrice;
    private int totalPrice;
    private char sc;

    public String getReferenceCode() {
        return referenceCode;
    }

    public void setReferenceCode(String referenceCode) {
        this.referenceCode = referenceCode;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getAdultCount() {
        return adultCount;
    }

    public void setAdultCount(int adultCount) {
        this.adultCount = adultCount;
    }

    public int getChildCount() {
        return childCount;
    }

    public void setChildCount(int childCount) {
        this.childCount = childCount;
    }

    public int getInfantCount() {
        return infantCount;
    }

    public void setInfantCount(int infantCount) {
        this.infantCount = infantCount;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public char getSc() {
        return sc;
    }

    public void setSc(char sc) {
        this.sc = sc;
    }

    public ArrayList<Passenger> getPassengers() {
        return Passengers;
    }

    public void setPassengers(ArrayList<Passenger> Passengers) {
        this.Passengers = Passengers;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    private ArrayList<Passenger> Passengers;
    private ArrayList<String> ticketNumbers = new ArrayList<String>();
    private Flight flight;

    public String getTicketNumber(int ticketNumber) {
        return ticketNumbers.get(ticketNumber);
    }

    public ArrayList<String> getTicketNumbers() {
        return ticketNumbers;
    }

    public void addTicketNumber(String ticketNumber) {
        this.ticketNumbers.add(ticketNumber);
    }

    public void setTicketNumber(ArrayList<String> ticketNumber) {
        this.ticketNumbers = ticketNumber;
    }

    public int getPrice(String type) {
        if(type.equals("adult"))
            return this.getAdultPrice();
        else if(type.equals("child"))
            return this.getChildPrice();
        else if(type.equals("infant"))
            return this.getInfantPrice();
        return -1;
    }

    public void setTypes(ArrayList<String> types) {
        for (int i = 0; i < this.getPassengers().size(); i++) {
            this.getPassengers().get(i).setType(types.get(i));
        }
    }

    public Reserve(Flight flight, ArrayList<Passenger> Passengers, String info,
                   String sc, int adultCount, int childCount, int infantCount) {
        this.flight = flight;
        this.Passengers = Passengers;
        this.sc = sc.charAt(0);
        this.adultCount = adultCount;
        this.childCount = childCount;
        this.infantCount = infantCount;
        List<String> infoSplit = Arrays.asList(info.split("\\s+"));
        adultPrice = Integer.parseInt(infoSplit.get(1));
        childPrice = Integer.parseInt(infoSplit.get(2));
        infantPrice = Integer.parseInt(infoSplit.get(3));
        totalPrice = adultPrice * adultCount + childPrice * childCount + infantPrice * infantCount;
        this.token = infoSplit.get(0);
    }
}
