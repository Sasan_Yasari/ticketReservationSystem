package common;

import serverInterface.Provider;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

public class ReserveRepository {
    public static final String CONN_STR = "jdbc:hsqldb:hsql://localhost/xdb";

    public String getReserveKey() {
        return reserveKey;
    }

    public void setReserveKey(String reserveKey) {
        this.reserveKey = reserveKey;
    }

    private String reserveKey;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    private String key;

    public char getSeat() {
        return seat;
    }

    public void setSeat(char seat) {
        this.seat = seat;
    }

    private char seat;

    private static ReserveRepository repository = new ReserveRepository();

    private ReserveRepository() {
    }

    public static ReserveRepository getRepository() {
        return repository;
    }

    static {
        try {
            Class.forName("org.hsqldb.jdbcDriver");
        } catch (ClassNotFoundException ex) {
                System.err.println("Unable to load HSQLDB JDBC driver");
        }
    }

    public void insertReserve(String flightKey, char seat) throws SQLException {
        Connection connection = DriverManager.getConnection(CONN_STR, "SA", "");
        Statement statement = connection.createStatement();
        this.reserveKey = keyGenerator();
        statement.executeQuery("insert into RESERVES (key, flightKey, seat) values('" + this.reserveKey + "','" + flightKey + "', '" + seat + "' )" );
        connection.close();
    }

    public void updateReserve(String token, String referenceCode, String username) throws SQLException {
        Connection connection = DriverManager.getConnection(CONN_STR, "SA", "");
        Statement statement = connection.createStatement();
        statement.executeQuery("update RESERVES set token='" + token +
                "', referenceCode='" + referenceCode +"', username='" + username +
                "' where key='" + this.reserveKey + "'");
        connection.close();
    }

    public void addPassenger(Passenger passenger, String ticketNumber) throws SQLException {
        Connection connection = DriverManager.getConnection(CONN_STR, "SA", "");
        Statement statement = connection.createStatement();
        statement.executeQuery("insert into PASSENGERS values('" + this.reserveKey + "','" + passenger.getFirstName() +
                "', '" + passenger.getSurname()+ "', " + passenger.getId() +
                ", '" + passenger.getType() + "', '" + ticketNumber + "')" );
        connection.close();
    }

    public Reserve providerReserve(Flight flight, ArrayList<Passenger> passengers) throws IOException, SQLException {
        Provider provider = new Provider();
        String response = provider.reserve(flight.getSource(), flight.getDestination(), flight.getDate(), flight.getAirlineCode(),
                flight.getFlightNumber(), flight.getSeatClass().getName(), Integer.toString(Repository.getRepository().getAdultCount()),
                Integer.toString(Repository.getRepository().getChildCount()), Integer.toString(Repository.getRepository().getInfantCount()), passengers);

        Reserve reserve = new Reserve(flight, passengers, response,
                flight.getSeatClass().getName()+Integer.toString(flight.getSeatClass().getCapacity()),
                Repository.getRepository().getAdultCount(), Repository.getRepository().getChildCount(), Repository.getRepository().getInfantCount());

        response = provider.finalize(reserve.getToken());
        List<String> responseSplit = Arrays.asList(response.split("\\s+"));
        reserve.setReferenceCode(responseSplit.get(0));
        for (int i = 1; i < responseSplit.size(); i++) {
            this.addPassenger(passengers.get(i - 1), responseSplit.get(i));
            reserve.addTicketNumber(responseSplit.get(i));
        }
        return reserve;
    }

    private String keyGenerator() {
        Random random = new Random();
        String key ="";
        for(int i = 0; i < 10; i++)
            key += (char) (65+random.nextInt(57));
        return key;
    }

    public ArrayList<Reserve> getAllTickets() throws SQLException {
        Connection connection = DriverManager.getConnection(CONN_STR, "SA", "");
        Statement statement = connection.createStatement();

        ResultSet reserveResultSet = statement.executeQuery("select * from reserves");
        ArrayList<Reserve> reserves = this.getReserves(reserveResultSet, statement);

        reserveResultSet.close();

        connection.close();
        return reserves;
    }

    public ArrayList<Reserve> getTickets(String username) throws SQLException {
        Connection connection = DriverManager.getConnection(CONN_STR, "SA", "");
        Statement statement = connection.createStatement();

        ResultSet reserveResultSet = statement.executeQuery("select * from reserves where username='" + username + "'");

        ArrayList<Reserve> reserves = this.getReserves(reserveResultSet, statement);

        reserveResultSet.close();
        connection.close();
        return reserves;
    }

    private ArrayList<Reserve> getReserves(ResultSet reserveResultSet, Statement statement) throws SQLException {
        ArrayList<Reserve> reserves = new ArrayList<>();
        ArrayList<Passenger> passengers = new ArrayList<>();

        while(reserveResultSet.next()) {
            Flight flight = Repository.getRepository().getFlight(reserveResultSet.getString("flightKey"),
                    reserveResultSet.getString("seat").charAt(0));

            ResultSet passengersResultSet = statement.executeQuery("select * from passengers" +
                    " where reservekey='"+ reserveResultSet.getString("key") + "'");

            while(passengersResultSet.next()) {
                Passenger passenger = new Passenger(passengersResultSet.getString("firstname"),
                        passengersResultSet.getString("surname"),
                        passengersResultSet.getString("id"),
                        passengersResultSet.getString("type"));
                passenger.setTicketNumber(passengersResultSet.getString("ticketnumber"));
                passengers.add(passenger);
            }
            passengersResultSet.close();

            String reserveInfo = reserveResultSet.getString("token") + " " +
                    flight.getSeatClass().getAdultPrice() + " " +
                    flight.getSeatClass().getChildPrice() + " " +
                    flight.getSeatClass().getInfantPrice();

            Reserve reserve = new Reserve(flight, passengers, reserveInfo, reserveResultSet.getString("seat"),
                    Repository.getRepository().getAdultCount(),
                    Repository.getRepository().getChildCount(),
                    Repository.getRepository().getInfantCount());
            reserves.add(reserve);
        }
        return reserves;
    }

    public Ticket getTicket(String id) throws SQLException {
        Connection connection = DriverManager.getConnection(CONN_STR, "SA", "");
        Statement statement = connection.createStatement();

        ResultSet passengerResultSet = statement
                .executeQuery("select * from passengers where id=" + id);
        if(!passengerResultSet.next())
            return null;

        ResultSet reserveResultSet = statement
                .executeQuery("select * from reserves where key='" +
                        passengerResultSet.getString("reserveKey") + "'");

        ArrayList<Reserve> reserves = this.getReserves(reserveResultSet, statement);

        Passenger passenger = new Passenger(passengerResultSet.getString("firstname"),
                passengerResultSet.getString("surname"),
                id, passengerResultSet.getString("type"));

        passengerResultSet.close();
        reserveResultSet.close();
        connection.close();
        return new Ticket(reserves.get(0).getFlight(), passenger);
    }

    public Ticket getTicket(String username, String id) throws SQLException {
        Connection connection = DriverManager.getConnection(CONN_STR, "SA", "");
        Statement statement = connection.createStatement();

        ResultSet userResultSet = statement
                .executeQuery("select * from users where username='" + username + "'");
        if(!userResultSet.next())
            return null;

        ResultSet reserveResultSet = statement
                .executeQuery("select * from reserves where key='" + userResultSet.getString("reserveKey") + "'");
        if(!reserveResultSet.next()) {
            return null;
        }
        ResultSet passengerResultSet = statement
                .executeQuery("select * from passengers where id=" + id);
        if(!passengerResultSet.next())
            return null;
        if(!passengerResultSet.getString("reserveKey").equals(userResultSet.getString("reserveKey")))
            return null;

        ArrayList<Reserve> reserves = this.getReserves(reserveResultSet, statement);


        Passenger passenger = new Passenger(passengerResultSet.getString("firstname"),
                passengerResultSet.getString("surname"),
                id, passengerResultSet.getString("type"));

        userResultSet.close();
        reserveResultSet.close();
        passengerResultSet.close();
        connection.close();
        return new Ticket(reserves.get(0).getFlight(), passenger);
    }
}
