package serverInterface;

import common.Passenger;

import java.io.IOException;
import java.util.ArrayList;

public class Provider {
    ServerInterface si;
    public Provider() throws IOException {
        si = new ServerInterface("178.62.207.47", 8081);
    }
    public int getLine() {
        return si.getLine();
    }
    public String search(String source, String destination, String date) throws IOException {
        return si.sendRequest("AV " + source + " " + destination + " " + date);
    }
    public String price(String source, String destination, String AirlineCode, char seat) throws IOException{
        return si.sendRequest("PRICE " + source + " " + destination + " "
                + AirlineCode + " " + seat);
    }
    public String reserve(String source, String destination, String date, String airlineCode,
                          int flightNumber, char seat, String adultCount, String childCount,
                          String infantCount, ArrayList<Passenger> passengers) throws IOException{
        String request = "RES " + source + " " + destination + " " + date + " " + airlineCode + " "
                + flightNumber + " " + seat + " " + adultCount + " " + childCount + " "
                + infantCount + "\n";
        for (int i = 0; i < passengers.size(); i++) {
            request += passengers.get(i).getFirstName()
                    + " " + passengers.get(i).getSurname()
                    + " " + passengers.get(i).getId() + "\n";
        }
        return si.sendRequest(request);
    }
    public String finalize(String token) throws IOException{
        return si.sendRequest("FIN " + token);
    }
    public void close() throws IOException {
        si.close();
    }
}
