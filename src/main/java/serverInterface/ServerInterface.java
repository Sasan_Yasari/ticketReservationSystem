package serverInterface;

import java.io.*;
import java.net.Socket;

public class ServerInterface {
    private Socket socket;
    private int line;

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public ServerInterface(String ip, int port) throws IOException{
        socket = new Socket(ip, port);
    }

    public String sendRequest(String query) throws IOException{
        line = 0;
        String response = "";
        PrintWriter pw = new PrintWriter(socket.getOutputStream(), true);
        pw.println(query);
        InputStream is = socket.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        while (!br.ready()) ;
        while (br.ready()) {
            response += br.readLine() + " \n";
            line++;
        }
        return response;
    }
    public void close() throws IOException {
        socket.close();
    }
}
