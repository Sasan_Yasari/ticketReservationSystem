package UI;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class UI {
    JFrame frame = new JFrame();
    JTextField fromS = new JTextField();
    JLabel fromS_ = new JLabel("from");
    JTextField toS = new JTextField();
    JLabel toS_ = new JLabel("to");
    JTextField dateS = new JTextField();
    JLabel dateS_ = new JLabel("date");
    JTextField adultS = new JTextField();
    JLabel adultS_ = new JLabel("adult");
    JTextField childS = new JTextField();
    JLabel childS_ = new JLabel("child");
    JTextField infantS = new JTextField();
    JLabel infantS_ = new JLabel("infant");
    JButton search = new JButton("search");
    JTextField fromR = new JTextField();
    JLabel fromR_ = new JLabel("from");
    JTextField toR = new JTextField();
    JLabel toR_ = new JLabel("to");
    JTextField dateR = new JTextField();
    JLabel dateR_ = new JLabel("date");
    JTextField airlineCodeR = new JTextField();
    JLabel airlineCodeR_ = new JLabel("airline");
    JTextField flightNumR = new JTextField();
    JLabel flightNumR_ = new JLabel("flightNum");
    JTextField seatClassR = new JTextField();
    JLabel seatClassR_ = new JLabel("SeatClass");
    JTextField adultR = new JTextField();
    JLabel adultR_ = new JLabel("adult");
    JTextField childR = new JTextField();
    JLabel childR_ = new JLabel("child");
    JTextField infantR = new JTextField();
    JLabel infantR_ = new JLabel("infant");
    JButton reserve = new JButton("Reserve");
    JTextField token = new JTextField();
    JLabel token_ = new JLabel("token");
    JButton finalize = new JButton("finalize");
    String response = "";
    public UI(){
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //search
        frame.add(fromS_);
        frame.add(toS_);
        frame.add(dateS_);
        frame.add(adultS_);
        frame.add(childS_);
        frame.add(infantS_);
        frame.add(fromS);
        frame.add(toS);
        frame.add(dateS);
        frame.add(adultS);
        frame.add(childS);
        frame.add(infantS);
        frame.add(search);

        fromS.setBounds(100, 10, 145, 20);
        toS.setBounds(100, 40, 145, 20);
        dateS.setBounds(100, 70, 145, 20);
        adultS.setBounds(100, 100, 145, 20);
        childS.setBounds(100, 130, 145, 20);
        infantS.setBounds(100, 160, 145, 20);
        fromS_.setBounds(10, 10, 50, 20);
        toS_.setBounds(10, 40, 50, 20);
        dateS_.setBounds(10, 70, 50, 20);
        adultS_.setBounds(10, 100, 50, 20);
        childS_.setBounds(10, 130, 50, 20);
        infantS_.setBounds(10, 160, 50, 20);
        search.setBounds(100, 190, 145, 20);

        //Reserve


        frame.add(fromR_);
        frame.add(toR_);
        frame.add(dateR_);
        frame.add(airlineCodeR_);
        frame.add(flightNumR_);
        frame.add(seatClassR_);
        frame.add(adultR_);
        frame.add(childR_);
        frame.add(infantR_);
        frame.add(fromR);
        frame.add(toR);
        frame.add(dateR);
        frame.add(airlineCodeR);
        frame.add(flightNumR);
        frame.add(seatClassR);
        frame.add(adultR);
        frame.add(childR);
        frame.add(infantR);
        frame.add(reserve);

        fromR.setBounds(360, 10, 145, 20);
        toR.setBounds(360, 40, 145, 20);
        dateR.setBounds(360, 70, 145, 20);
        airlineCodeR.setBounds(360, 100, 145, 20);
        flightNumR.setBounds(360, 130, 145, 20);
        seatClassR.setBounds(360, 160, 145, 20);
        adultR.setBounds(360, 190, 145, 20);
        childR.setBounds(360, 220, 145, 20);
        infantR.setBounds(360, 250, 145, 20);

        fromR_.setBounds(280, 10, 80, 20);
        toR_.setBounds(280, 40, 80, 20);
        dateR_.setBounds(280, 70, 80, 20);
        airlineCodeR_.setBounds(280, 100, 80, 20);
        flightNumR_.setBounds(280, 130, 80, 20);
        seatClassR_.setBounds(280, 160, 80, 20);
        adultR_.setBounds(280, 190, 80, 20);
        childR_.setBounds(280, 220, 80, 20);
        infantR_.setBounds(280, 250, 80, 20);
        reserve.setBounds(360, 280, 145, 20);


        //finalize

        frame.add(token);
        frame.add(token_);
        frame.add(finalize);

        token.setBounds(590, 10, 145, 20);
        token_.setBounds(530, 10, 50, 20);
        finalize.setBounds(590, 40, 145, 20);

        frame.setLayout(null);

        frame.setSize(800, 350);
        frame.setVisible(true);
    }
    public void search(){
        search.addActionListener((ActionEvent event) -> {
            response = "search " + fromS.getText() + " " + toS.getText() + " "
                    + adultS.getText() + " " + childS.getText() + " " + infantS.getText();

            JFrame searchResult = new JFrame();
            searchResult.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            JOptionPane.showMessageDialog(searchResult, response);
        });
    }
    public void reserve(){
        reserve.addActionListener((ActionEvent event) -> {
            JFrame passengerFrame = new JFrame();
            passengerFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            int passengerCount = Integer.parseInt(adultR.getText())
                    + Integer.parseInt(childR.getText())
                    + Integer.parseInt(infantR.getText());
            for(int i = 0; i < passengerCount; i++){
                JTextField firstName = new JTextField();
                JLabel firstName_ = new JLabel("firstName");
                JTextField surname = new JTextField();
                JLabel surname_ = new JLabel("surname");
                JTextField ID = new JTextField();
                JLabel ID_ = new JLabel("ID");
                passengerFrame.add(firstName);
                passengerFrame.add(firstName_);
                passengerFrame.add(surname);
                passengerFrame.add(surname_);
                passengerFrame.add(ID);
                passengerFrame.add(ID_);

                firstName.setBounds(90, i * 100 + 10, 145, 20);
                surname.setBounds(90, i * 100 + 40, 145, 20);
                ID.setBounds(90, i * 100 + 70, 145, 20);

                firstName_.setBounds(10, i * 100 + 10, 80, 20);
                surname_.setBounds(10, i * 100 + 40, 80, 20);
                ID_.setBounds(10, i * 100 + 70, 80, 20);
            }
            JButton add = new JButton("add Passenger");
            passengerFrame.add(add);
            add.setBounds(90, passengerCount * 100 + 10, 145, 20);
            passengerFrame.setSize(300, passengerCount * 120);
            passengerFrame.setLayout(null);
            passengerFrame.setVisible(true);

            JFrame tokenFrame = new JFrame();
            tokenFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            add.addActionListener((ActionEvent e) ->{
                passengerFrame.setVisible(false);
                JOptionPane.showMessageDialog(tokenFrame, "............token...............");
            });
        });
    }
    public void finalize(){
        JFrame ticketFrame = new JFrame();
        ticketFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        finalize.addActionListener((ActionEvent event) -> {
            JOptionPane.showMessageDialog(ticketFrame, "............ticket information...............");
        });
    }
}
