package myServer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class MyServer {
    private ServerSocket server;
    private Socket socket;

    public MyServer(int port) throws IOException{
        server = new ServerSocket(port);
        socket = server.accept();
    }
    public String readQuery() throws IOException{
        DataInputStream dis = new DataInputStream(socket.getInputStream());
        return dis.readLine();
    }

    public void writeToClient(String out) throws IOException{
        DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
        dos.writeChars(out);
    }
}
