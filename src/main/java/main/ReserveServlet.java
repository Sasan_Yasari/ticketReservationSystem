package main;

import common.Flight;
import common.Repository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import org.apache.log4j.Logger;


public class ReserveServlet extends HttpServlet{
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        Logger LOGGER = Logger.getLogger(ReserveServlet.class.getName());
//        Flight flight = Repository.getRepository().getFlight(Integer.parseInt(req.getParameter("key")));
        Flight flight = new Flight();
        LOGGER.debug("selected seat class: " + req.getParameter("seat"));
        try {
            LOGGER.info("TMPRES " + flight.getKey() + " "
                    + flight.getAdultPrice() + " "
                    + flight.getChildPrice() + " "
                    + flight.getInfantPrice());
        }catch (NullPointerException e) {
            req.setAttribute("message", "flight not found");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/errorPage.jsp");
            dispatcher.forward(req, res);
        }
        req.getRequestDispatcher("reserve.html").forward(req, res);
    }
}
