package main;

import common.Flight;
import common.Passenger;
import common.Repository;
import common.Reserve;
import serverInterface.Provider;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.log4j.Logger;


public class TicketServlet extends HttpServlet{
    final static Logger LOGGER = Logger.getLogger(TicketServlet.class.getName());
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        Provider provider = new Provider();
        Flight flight = new Flight();
//        Flight flight = Repository.getRepository().getFlight(Integer.parseInt(req.getParameter("key")));
        int adultCount = Integer.parseInt(req.getParameter("adultCount"));
        int childCount = Integer.parseInt(req.getParameter("childCount"));
        int infantCount = Integer.parseInt(req.getParameter("infantCount"));
        LOGGER.debug("passengers count: " + adultCount + " " + childCount + " " +infantCount);
        String request = "";
        ArrayList<Passenger> passengers = new ArrayList<Passenger>();
        for (int i = 0; i < adultCount + childCount + infantCount; i++) {
            Passenger pass = new Passenger(req.getParameter("first-name" + i),
                    req.getParameter("last-name" + i),
                    req.getParameter("ID" + i),
                    req.getParameter("gender" + i));
            passengers.add(pass);
        }
        String response = provider.reserve(flight.getSource(), flight.getDestination(), flight.getDate(), flight.getAirlineCode(),
                flight.getFlightNumber(), req.getParameter("seat").charAt(0), req.getParameter("adultCount"),
                req.getParameter("childCount"), req.getParameter("infantCount"), passengers);
        LOGGER.debug("selected seat class: " + req.getParameter("seat"));
        Reserve reserve = new Reserve(flight, passengers, response, req.getParameter("seat"),
                adultCount, childCount, infantCount);
        LOGGER.info("RES " + reserve.getFlight().getKey() + " " + reserve.getToken() + " "
                + adultCount + " " + childCount + " " +infantCount);
        response = provider.finalize(reserve.getToken());
        List<String> responseSplit = Arrays.asList(response.split("\\s+"));
        reserve.setReferenceCode(responseSplit.get(0));
        LOGGER.info("FINRES " + reserve.getToken() + " "
                + reserve.getReferenceCode() + " "
                + reserve.getTotalPrice());
        for (int i = 1; i < responseSplit.size(); i++) {
            reserve.addTicketNumber(responseSplit.get(i));
        }
        int i, j, k;
        for(i = 0; i < adultCount; i++){
            LOGGER.info("TICKET " + reserve.getReferenceCode() + " "
                    + reserve.getTicketNumber(i) + " "
                    + reserve.getPassengers().get(i).getId() + " "
                    + reserve.getPassengers().get(i).getType() + " "
                    + reserve.getAdultPrice());
        }
        for(j = i; j < childCount + i; j++){
            LOGGER.info("TICKET " + reserve.getReferenceCode() + " "
                    + reserve.getTicketNumber(j) + " "
                    + reserve.getPassengers().get(j).getId() + " "
                    + reserve.getPassengers().get(j).getType() + " "
                    + reserve.getChildPrice());
        }
        for(k = j; k < infantCount + j; k++){
            LOGGER.info("TICKET " + reserve.getReferenceCode() + " "
                    + reserve.getTicketNumber(k) + " "
                    + reserve.getPassengers().get(k).getId() + " "
                    + reserve.getPassengers().get(k).getType() + " "
                    + reserve.getInfantPrice());
        }
        req.setAttribute("reserve", reserve);
        req.getRequestDispatcher("ticket.html").forward(req, res);

    }
}
