package main;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import common.*;
import serverInterface.Provider;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import org.apache.log4j.Logger;

public class SearchServlet extends HttpServlet {
    ArrayList<Flight> flights = new ArrayList<>();
    Logger LOGGER = Logger.getLogger(SearchServlet.class.getName());

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws IOException, ServletException {
        Provider provider = new Provider();
        String response = provider.search(req.getParameter("source"),
                req.getParameter("destination"),
                req.getParameter("date"));
        LOGGER.info( "SRCH " + req.getParameter("source")+ " "
                + req.getParameter("destination") + " "
                + req.getParameter("date"));
        List<String> responseSplit = Arrays.asList(response.split("\\r?\\n"));
        int responses = provider.getLine()/2 + 1;
        res.setContentType("text/html; charset=UTF-8");
        int adultCount = Integer.parseInt(req.getParameter("adult"));
        int childCount = Integer.parseInt(req.getParameter("child"));
        int infantCount = Integer.parseInt(req.getParameter("infant"));
        LOGGER.debug("passengers count: " + adultCount + " " + childCount + " " +infantCount);
        for (int j = 0; j < responses; j += 2) {
            Flight f = new Flight();
            if (req.getParameter("source").equals(req.getParameter("destination")))
                throw new InvalidParameterException();
            List<String> seats = Arrays.asList(responseSplit.get(j + 1).split("\\s+"));
            for(int k = 0; k < seats.size(); k++) {
                if (seats.get(k).charAt(1) != 'C') {
                    List<String> resSplit = Arrays.asList(responseSplit.get(j).split("\\s+"));
                    String prices = provider.price(req.getParameter("source"),
                            req.getParameter("destination"), resSplit.get(0), seats.get(k).charAt(0));
                    List<String> pricesSplit = Arrays.asList(prices.split("\\s+"));
                    SeatClass sc = new SeatClass(seats.get(k),
                            req.getParameter("source"),
                            req.getParameter("destination"),
                            responseSplit.get(0),
                            Integer.parseInt(pricesSplit.get(0)),
                            Integer.parseInt(pricesSplit.get(1)),
                            Integer.parseInt(pricesSplit.get(2)));
                    f = new Flight(responseSplit.get(j), sc);
                    f.getKey();
                    flights.add(f);
//                    Repository.getRepository().addFlight(f);
                }
            }
        }
        PrintWriter out = res.getWriter();
        out.println(
                "<!DOCTYPE html>\n" +
                        "<html lang=\"fa\" dir = \"rtl\">\n" +
                        "<head>\n" +
                        "    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">\n" +
                        "    <link rel=\"stylesheet\" href=\"css/result.css\">\n" +
                        "    <meta charset=\"UTF-8\">\n" +
                        "    <title>اکبر تیکت</title>\n" +
                        "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                        "    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">\n" +
                        "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js\"></script>\n" +
                        "    <script src=\"https://unpkg.com/babel-standalone@6/babel.js\"></script>\n" +
                        "    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>\n" +
                        "</head>\n" +
                        "<body>\n" +
                        "<script src=\"js/result.js\"></script>" +
                        "<div id=\"head\" class=\"row\">\n" +
                        "    <div class=\"col-xs-0 col-md-2\"></div>\n" +
                        "    <div id=\"user\" class=\"col-xs-5 col-md-2\">\n" +
                        "        <span><i class=\"fa fa-user fa-lg \" aria-hidden=\"true\" ></i> نام کاربر </span>\n" +
                        "        <div class=\"user-dropdown\">\n" +
                        "            <a id=\"ticket\" href=\"#\">بلیط های من</a><br>\n" +
                        "            <a id=\"exit\" href=\"#\">خروج</a>\n" +
                        "        </div>\n" +
                        "    </div>\n" +
                        "    <div class=\"col-xs-0 col-md-4\"></div>\n" +
                        "    <div class=\"col-xs-7 col-md-2\">\n" +
                        "      <a href=\"home.html\">\n" +
                        "      <img id = \"logo-image\" alt=\"logo\" src= \"images/assets/LogoBlack.png\" >\n" +
                        "          <div class=\"logo-text\">اکبر تیکت</div>\n" +
                        "            <i class=\"fa fa-registered fa-x\" aria-hidden=\"true\" ></i>\n" +
                        "        </a>\n" +
                        "    </div>\n" +
                        "    <div class=\"col-xs-0 col-md-2\"></div>\n" +
                        "</div>\n" +
                        "<div id=\"navparent\" class=\"row\">\n" +
                        "    <div id=\"nav\" class=\"col-xs-12 col-md-12\">\n" +
                        "            <a class=\"links\" href=\"home.html\">جستجوی پرواز &nbsp;</a>" +
                        "<i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>&nbsp;" +
                        "<a class=\"page\" href=\"#\"> انتخاب پرواز &nbsp;</a> " +
                        "<i id=\"page\" class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>&nbsp;" +
                        "<a class=\"links\" href=\"reserve.html\">  ورود اطلاعات &nbsp;</a>  " +
                        "<i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i> &nbsp;" +
                        "<a class=\"links\" href=\"ticket.html\"> دریافت بلیت</a>\n" +
                        "    </div>\n" +
                        "</div>\n" +
                        "<div id=\"back\" class=\"row\">\n" +
                        "    <div class=\"col-xs-0 col-md-2\"></div>\n" +
                        "    <div id=\"text-above\" class=\"col-xs-12 col-md-8\">\n" +
                        "        <div class=\"col-xs-12 col-md-7 col-md-push-5\">\n" +
                        "            <a class=\"back-link\" href=\"home.html \"><i class=\"fa fa-arrow-right\" aria-hidden=\"true\"></i>  برگشت به صفحه\u200Cی جستجو</a>\n" +
                        "        </div>\n" +
                        "        <div class=\"col-xs-12 col-md-5 col-md-pull-8\">"+ flights.size() +" پرواز با مشخصات دلخواه شما پیدا شد </div>\n" +
                        "    </div>\n" +
                        "    <div class=\"col-xs-0 col-md-2\"></div>\n" +
                        "</div>" +
                        "<div class = \"row\">\n" +
                        "  <div class=\"col-xs-0 col-md-3\"></div>\n" +
                        "  <div class = \"col-xs-12 col-md-7\" id = \"filter\" >\n" +
                        " مرتب سازی بر اساس: " +
                        "      <button class=\"sort\" onclick=\"price_lh();\">قیمت صعودی</button>\n" +
                        "      <button class=\"sort\" onclick=\"price_hl();\">قیمت نزولی</button>\n");
                        for(int i = 0; i < flights.size(); i++) {
                            if(!repeated(flights, i))
                                out.println("      <button class=\"sort\" onclick=\"seat(\'" + flights.get(i).getSeatClass().getName() +"\');\"> صندلی کلاس" + flights.get(i).getSeatClass().getName() + "</button>\n");
                        }
                        for(int i = 0; i < flights.size(); i++) {
                            if(!repeatedAirline(flights, i))
                                out.println("      <button class=\"sort\" onclick=\"airline(\'" + flights.get(i).getAirlineCode() +"\');\"> هواپیمائی " + flights.get(i).getAirlineCode() + "</button>\n");
                        }
                        out.println("  </div>\n" +
                        "  <div class=\"col-xs-0 col-md-2\"></div>\n" +
                        "</div>" +
                        "\n <div id=\"gray\" class=\"row\">\n" +
                        "    <div class=\"col-xs-0 col-md-2\"></div>\n" +
                        "    <div id=\"gray-table\" class=\"col-xs-12 col-md-8\">\n" +
                        "        <div class=\"row\">"
        );
        for(int j = 0; j < flights.size(); j++) {
            out.println("<div id=\"white" + j + "\" class=\"col-xs-12 col-md-12\">\n " +
                    "                <div class=\"col-xs-3 col-md-3 col-md-push-9\">\n" +
                    "                    <div id = \"title"+ j +"\" class=\"rows title\">" + flights.get(j).getAirlineCode() + " " + flights.get(j).getFlightNumber() + " </div>\n" +
                    "                    <div id = \"date"+ j +"\" class=\"rows\"><i class=\"fa fa-calendar-o\" aria-hidden=\"true\"></i> " + flights.get(j).getDate() + "</div>\n" +
                    "                </div>\n" +
                    "                <div id=\"loc"+ j +"\" class=\"col-xs-6 col-md-6\"> " + flights.get(j).getPersianSource() + " " + flights.get(j).getDeparture() +
                    " <i class=\"fa fa-angle-double-left\" aria-hidden=\"true\"></i> " + flights.get(j).getPersianDestination() + " " + flights.get(j).getArrival() + " \n" +
                    "                  <div class=\"rows\">\n" +
                    "                    <i id=\"from-logo\" class=\"fa fa-plane fa-lg\" aria-hidden=\"true\"></i> <span class = \"num\"> " + flights.get(j).getAirplaneModel() + " </span>\n" +
                    "                    <i id = \"fr-lg\" class=\"fa fa-suitcase\" aria-hidden=\"true\"></i> <span class = \"num\"> " + flights.get(j).getSeatClass().getCapacity() + "  صندلی باقیمانده کلاس " + "<div class=\"sc\">" + flights.get(j).getSeatClass().getName() +
                    "</div></span>\n</div>\n" +
                    "                </div>\n" +
                    "                <div id=\"red-but"+ j +"\" class=\"col-xs-3 col-md-3 col-md-pull-9\">\n" +
                    "<div class=\"rows\">" +
                    "<form action=\"reserve\" method=\"POST\">" +
                    "<input name=\"flights\" type=\"hidden\" value=\"" + flights.size() + "\"/>" +
                    "<input name=\"adultCount\" type=\"hidden\" value=\"" + adultCount + "\"/>" +
                    "<input name=\"childCount\" type=\"hidden\" value=\"" + childCount + "\"/>" +
                    "<input name=\"infantCount\" type=\"hidden\" value=\"" + infantCount + "\"/>" +
                    "<input name=\"seat\" type=\"hidden\" value=\"" + flights.get(j).getSeatClass().getName() + flights.get(j).getSeatClass().getCapacity() + "\"/>" +
                    "<input name=\"key\" type=\"hidden\" value=\"" + flights.get(j).getKey() + "\"/>" +
                    "<input class=\"totalPrice\" type=\"hidden\" value=\"" + flights.get(j).getPrice(adultCount, childCount, infantCount) + "\"/>" +
                    "<button class = \"but\" type=\"Reserve\" >" + flights.get(j).getPrice(adultCount, childCount, infantCount) + " ریال " +
                    " <br> " + "رزرو آنلاین" + "</button></form></div>" +
                    "                </div>\n" +
                    "            </div>");
        }
        out.println("<div class=\"col-xs-0 col-md-2\"></div>\n</div>\n</div>\n</div>\n</body>\n</html>");
        provider.close();
        flights.clear();
    }

    private boolean repeated(ArrayList<Flight> flights, int i) {
        for(int j = 0; j < i; j++)
                if (flights.get(i).getSeatClass().getName() == flights.get(j).getSeatClass().getName())
                    return true;
        return false;
    }
    private boolean repeatedAirline(ArrayList<Flight> flights, int i) {
        for(int j = 0; j < i; j++)
            if (flights.get(i).getAirlineCode().equals(flights.get(j).getAirlineCode()))
                return true;
        return false;
    }
}
