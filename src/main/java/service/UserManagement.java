package service;


import common.LoginInfo;
import common.PassengerRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.SQLException;

@Path("/usermanagement")
public class UserManagement {

    @Context
    private HttpServletRequest request;

    @POST
    @Path("/login")
    @Produces(MediaType.TEXT_PLAIN)
    public String login(LoginInfo loginInfo) throws SQLException {
        boolean result = PassengerRepository.getRepository().getPassengerInfo(loginInfo);
        HttpSession session = request.getSession();

        if (result) {
            session.setAttribute("username", loginInfo.getUsername());
            PassengerRepository.getRepository().addSessionID(loginInfo.getUsername(), session.getId());
        }
        if (result) {
            if (session.getAttribute("username").equals("admin"))
                return "admin";
            else
                return "user";
        }
        return "";
    }
    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(LoginInfo loginInfo) throws SQLException {
        HttpSession session = request.getSession();
        boolean result = PassengerRepository.getRepository().addAccount(loginInfo, session.getId());
        return Response.status(200).entity(result).build();
    }

    @POST
    @Path("/user")
    @Produces(MediaType.TEXT_PLAIN)
    public String user() throws SQLException {
        HttpSession session = request.getSession();
        if(session.getAttribute("username") == null ||
                PassengerRepository.getRepository().getSessionId((String) session.getAttribute("username")) == null)
            return "";
        if(PassengerRepository.getRepository().getSessionId((String) session.getAttribute("username")).equals(session.getId())
                && session.getAttribute("username").equals("admin"))
            return "admin";
        if(PassengerRepository.getRepository().getSessionId((String) session.getAttribute("username")).equals(session.getId()))
            return "user";
        return "";
    }

    @POST
    @Path("/logout")
    public void logout() throws SQLException {
        HttpSession session = request.getSession();
        PassengerRepository.getRepository().logout((String) session.getAttribute("username"));
        session.setAttribute("username", "");
    }

}
