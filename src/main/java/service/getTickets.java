package service;

import common.Reserve;
import common.ReserveRepository;
import common.Ticket;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.ArrayList;

@Path("/tickets")
public class getTickets {

    @Context
    private HttpServletRequest request;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Reserve> getTickets() throws SQLException {
        HttpSession session = request.getSession();
        if(session.getAttribute("username").equals("admin")) {
            return ReserveRepository.getRepository().getAllTickets();
        }
        else if (session.getAttribute("username") != null){
            return  ReserveRepository.getRepository().getTickets((String) session.getAttribute("username"));
        }

        return null;
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Ticket getTickets(@PathParam("id") String id) throws SQLException {
        HttpSession session = request.getSession();
        if(session.getAttribute("username").equals("admin")) {
            System.out.println("admin");
            return ReserveRepository.getRepository().getTicket(id.substring(3));
        }
        else if (session.getAttribute("username") != null){
            System.out.println("user");
            return  ReserveRepository.getRepository().getTicket(
                    (String) session.getAttribute("username"), id.substring(3));
        }

        return null;
    }

}
