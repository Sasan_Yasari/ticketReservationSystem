package service;


import common.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

@Path("reserve")
public class ReserveWS {

    @Context
    private HttpServletRequest request;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Flight reserve(FlightSelect flightSelect) throws IOException, SQLException {

        Logger LOGGER = Logger.getLogger(ReserveWS.class.getName());

        ReserveRepository.getRepository().setKey(flightSelect.getKey());
        ReserveRepository.getRepository().setSeat(flightSelect.getSeat());

        Flight flight = Repository.getRepository().getFlight(flightSelect.getKey(), flightSelect.getSeat());

        flight.setPrevPrice(flightSelect.getPrice());

        List<String> pricesSplit = Repository.getRepository().updatePrices(flight, flightSelect);

        flight.setTotalPrice(Integer.parseInt(pricesSplit.get(0)) * Repository.getRepository().getAdultCount() +
                Integer.parseInt(pricesSplit.get(1)) * Repository.getRepository().getChildCount() +
                Integer.parseInt(pricesSplit.get(2)) * Repository.getRepository().getInfantCount());

        LOGGER.debug("selected seat class: " + flight.getSeatClass().getName());
        LOGGER.info("TMPRES " + flight.getKey() + " "
                + flight.getAdultPrice() + " "
                + flight.getChildPrice() + " "
                + flight.getInfantPrice());

        ReserveRepository.getRepository().insertReserve(flightSelect.getKey(), flightSelect.getSeat());

        HttpSession session = request.getSession();
        PassengerRepository.getRepository().addReserveKey(
                ReserveRepository.getRepository().getReserveKey(), (String) session.getAttribute("username"));

        return flight;
    }
}
