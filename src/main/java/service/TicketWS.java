package service;

import common.*;
import serverInterface.Provider;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.log4j.Logger;

@Path("ticket")
public class TicketWS {

    @Context
    private HttpServletRequest request;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Reserve TicketWS(ArrayList<Passenger> passengers) throws IOException, SQLException {
        Logger LOGGER = Logger.getLogger(TicketWS.class.getName());


        Flight flight = Repository.getRepository().getFlight(ReserveRepository.getRepository().getKey(), ReserveRepository.getRepository().getSeat());
        LOGGER.debug("passengers count: " + Repository.getRepository().getAdultCount() +
                " " + Repository.getRepository().getChildCount() +
                " " + Repository.getRepository().getInfantCount());
        LOGGER.debug("selected seat class: " + flight.getSeatClass().getName());

        Reserve reserve = ReserveRepository.getRepository().providerReserve(flight, passengers);

        ReserveRepository.getRepository().updateReserve(reserve.getToken(),
                reserve.getReferenceCode(), (String) request.getSession().getAttribute("username"));

        LOGGER.info("RES " + reserve.getFlight().getKey() + " " + reserve.getToken() +
                " " + Repository.getRepository().getAdultCount() +
                " " + Repository.getRepository().getChildCount() +
                " " + Repository.getRepository().getInfantCount());
        LOGGER.info("FINRES " + reserve.getToken() + " "
                + reserve.getReferenceCode() + " "
                + reserve.getTotalPrice());
        int i, j, k;
        for(i = 0; i < Repository.getRepository().getAdultCount(); i++){
            LOGGER.info("TICKET " + reserve.getReferenceCode() + " "
                    + reserve.getTicketNumber(i) + " "
                    + reserve.getPassengers().get(i).getId() + " "
                    + reserve.getPassengers().get(i).getType() + " "
                    + reserve.getAdultPrice());
        }
        for(j = i; j < Repository.getRepository().getChildCount() + i; j++){
            LOGGER.info("TICKET " + reserve.getReferenceCode() + " "
                    + reserve.getTicketNumber(j) + " "
                    + reserve.getPassengers().get(j).getId() + " "
                    + reserve.getPassengers().get(j).getType() + " "
                    + reserve.getChildPrice());
        }
        for(k = j; k < Repository.getRepository().getInfantCount() + j; k++){
            LOGGER.info("TICKET " + reserve.getReferenceCode() + " "
                    + reserve.getTicketNumber(k) + " "
                    + reserve.getPassengers().get(k).getId() + " "
                    + reserve.getPassengers().get(k).getType() + " "
                    + reserve.getInfantPrice());
        }
        return reserve;

    }
}
