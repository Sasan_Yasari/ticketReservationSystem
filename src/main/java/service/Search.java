package service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import common.Flight;
import common.Repository;
import common.SearchRequest;
import org.apache.log4j.Logger;

@Path("search")
public class Search {

    @Context
    private HttpServletRequest request;
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ArrayList<Flight> search(SearchRequest searchRequest) throws IOException, SQLException {

        Repository.getRepository().setAdultCount(searchRequest.getAdultCount());
        Repository.getRepository().setChildCount(searchRequest.getChildCount());
        Repository.getRepository().setInfantCount(searchRequest.getInfantCount());

        Logger LOGGER = Logger.getLogger(Search.class.getName());

        LOGGER.info( "SRCH " + searchRequest.getSource() + " " + searchRequest.getDestination() +
                " " + searchRequest.getDate());
        LOGGER.debug("passengers count: " + searchRequest.getAdultCount() + " "
                + searchRequest.getChildCount()+ " " + searchRequest.getInfantCount());
        return Repository.getRepository().getFlights(searchRequest.getSource(), searchRequest.getDestination(), searchRequest.getDate());
    }
}
