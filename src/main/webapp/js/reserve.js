
var i, j, k;
function set(){
    i = document.getElementById('adultIndex').value;
    j = document.getElementById('childIndex').value;
    k = document.getElementById('infantIndex').value;
}
function inc_adult() {
    if(document.getElementsByName('seat')[0].value.charAt(1) === 'A'
        || document.getElementsByName('seat')[0].value.charAt(1) > k){
        i++;
        j++;
        k++;
        price_update('adult', i, j, k);
        document.getElementById('adult-person').innerHTML += "<div class=\"row person\" id=\"adult"+ (i - 1) +"\"><div class=\"col-xs-12 col-md-2 col-md-push-10 name\"><i class=\"fa fa-male fa-2x\" aria-hidden=\"true\"></i> " + i + "- بزرگسال:</div><div class=\"col-xs-12 col-md-1 col-md-push-7 gender\"><select name=\"gender" +(i-1) + "\"><option value=\"MR\"> آقای </option><option value=\"MRS\"> خانم </option></select></div><div class=\"col-xs-12 col-md-3 col-md-push-3\"><input name=\"first-name" + (i-1) + "\"class=\"in\" type=\"text\" placeholder=\"نام(انگلیسی)\"></div><div class=\"col-xs-12 col-md-3 col-md-pull-3\"><input name=\"last-name" + (i-1) + "\" class=\"in\" type=\"text\" placeholder=\"نام خانوادگی(انگلیسی)\"></div><div class=\"col-xs-12 col-md-3 col-md-pull-9\"><input name=\"ID" + (i-1) + "\" class=\"in\" type=\"text\" placeholder=\"شماره ملی\"></div></div>";
        info_update('child', j, i);
        info_update('infant', k, j);
    }
    else alert('ظرفیت تکمیل است');
}

function dec_adult() {
    if(i !== 0){
        i--;
        j--;
        k--;
        price_update('adult', i, j, k);
        document.getElementById('adult' + i).innerHTML = "";
        info_update('child', j, i);
        info_update('infant', k, j);
    }
}
function inc_child() {
    if(document.getElementsByName('seat')[0].value.charAt(1) === 'A'
        || document.getElementsByName('seat')[0].value.charAt(1) > k){
        j++;
        k++;
        price_update('child', i, j, k);
        document.getElementById('child-person').innerHTML += "<div class=\"row person\" id=\"child"+ (j-i-1) +"\"><div class=\"col-xs-12 col-md-2 col-md-push-10 name\"><i class=\"fa fa-child fa-2x\" aria-hidden=\"true\"></i> " + j + "- خردسال:</div><div class=\"col-xs-12 col-md-1 col-md-push-7 gender\"><select name=\"gender" +(j-1) + "\"><option value=\"MR\"> آقای </option><option value=\"MRS\"> خانم </option></select></div><div class=\"col-xs-12 col-md-3 col-md-push-3\"><input name=\"first-name" + (j-1) + "\"class=\"in\" type=\"text\" placeholder=\"نام(انگلیسی)\"></div><div class=\"col-xs-12 col-md-3 col-md-pull-3\"><input name=\"last-name" + (j-1) + "\" class=\"in\" type=\"text\" placeholder=\"نام خانوادگی(انگلیسی)\"></div><div class=\"col-xs-12 col-md-3 col-md-pull-9\"><input name=\"ID" + (j-1) + "\" class=\"in\" type=\"text\" placeholder=\"شماره ملی\"></div></div>";
        info_update('infant', k, j);
    }
    else alert('ظرفیت تکمیل است');
}
function dec_child() {
    if((j - i) !== 0) {
        j--;
        k--;
        price_update('child', i, j, k);
        document.getElementById('child' + (j-i)).innerHTML = "";
        info_update('infant', k, j);
    }
}
function inc_infant() {
    if(document.getElementsByName('seat')[0].value.charAt(1) === 'A'
        || document.getElementsByName('seat')[0].value.charAt(1) > k){
    k++;
    price_update('infant', i, j, k);
    document.getElementById('infant-person').innerHTML += "<div class=\"row person\" id=\"infant"+ (k-j-1) +"\"><div class=\"col-xs-12 col-md-2 col-md-push-10 name\"><i class=\"fa fa-child fa-lg\" aria-hidden=\"true\"></i>&nbsp; &nbsp; " + k + "- نوزاد:</div><div class=\"col-xs-12 col-md-1 col-md-push-7 gender\"><select name=\"gender" +(k-1) + "\"><option value=\"MR\"> آقای </option><option value=\"MRS\"> خانم </option></select></div><div class=\"col-xs-12 col-md-3 col-md-push-3\"><input name=\"first-name" + (k-1) + "\"class=\"in\" type=\"text\" placeholder=\"نام(انگلیسی)\"></div><div class=\"col-xs-12 col-md-3 col-md-pull-3\"><input name=\"last-name" + (k-1) + "\" class=\"in\" type=\"text\" placeholder=\"نام خانوادگی(انگلیسی)\"></div><div class=\"col-xs-12 col-md-3 col-md-pull-9\"><input name=\"ID" + (k-1) + "\" class=\"in\" type=\"text\" placeholder=\"شماره ملی\"></div></div>";
    }
    else alert('ظرفیت تکمیل است');
}
function dec_infant() {
    if((k - j) !== 0) {
         k--;
         price_update('infant', i, j, k);
         document.getElementById('infant' + (k-j)).innerHTML = "";
    }
}
function price_update(type, i, j, k) {
    var index;
    if(type === 'adult')
        index = i;
    if(type === 'child')
        index = j - i;
    if(type === 'infant')
        index = k - j;
    document.getElementById(type + 'Count').innerHTML =  index + ' نفر';
    document.getElementById(type + 'sPrice').innerHTML = index * document.getElementById(type + 'Price').value + ' ریال';
    document.getElementById('sum').innerHTML = i * document.getElementById('adultPrice').value
                                             + (j - i) * document.getElementById('childPrice').value
                                             + (k - j) * document.getElementById('infantPrice').value + ' ریال';
}
function info_update(type, k, j) {
    for(index = 0; index < k-j; index++){
        if(type === 'infant')
            document.getElementById(type + index).getElementsByClassName('name')[0].innerHTML = "<i class=\"fa fa-child fa-lg\" aria-hidden=\"true\"></i>&nbsp; &nbsp;"+ (j+1+index)+"- نوزاد:";
        else
            document.getElementById(type + index).getElementsByClassName('name')[0].innerHTML = "<i class=\"fa fa-child fa-2x\" aria-hidden=\"true\"></i>"+ (j+1+index)+"- خردسال:";
        document.getElementById(type + index).getElementsByClassName('gender')[0].innerHTML = "<select name=\"gender"+(j+index)+"\"><option value=\"MR\"> آقای </option><option value=\"MRS\"> خانم </option></select>";
        document.getElementById(type + index).getElementsByClassName('f-name')[0].innerHTML = "<input name=\"first-name"+ (j+index) + "\" class=\"in\" type=\"text\" placeholder=\"نام(انگلیسی)\">";
        document.getElementById(type + index).getElementsByClassName('l-name')[0].innerHTML = "<input name=\"last-name"+ (j+index) + "\" class=\"in\" type=\"text\" placeholder=\"نام خانوادگی(انگلیسی)\">";
        document.getElementById(type + index).getElementsByClassName('ID-num')[0].innerHTML = "<input name=\"ID"+ (j+index) + "\" class=\"in\" type=\"text\" placeholder=\"شماره ملی\">";
    }
}
function checkThenSubmit() {
    for(index = 0; index < k; index++){
        if(document.getElementsByName('first-name'+index)[0].value.length < '2') {
            alert('نام باید بیشتر از ۲ حرف باشد');
            return;
        }
        var format = /^[A-Za-z.\s_-]+$/;
        if(!format.test(document.getElementsByName('first-name'+index)[0].value)){
            alert('فقط از حروف انگلیسی استفاده کنید');
            return;
        }
        if(document.getElementsByName('last-name'+index)[0].value.length < '2') {
            alert('نام خانوادگی باید بیشتر از ۲ حرف باشد');
            return;
        }
        if(!format.test(document.getElementsByName('last-name'+index)[0].value)){
            alert('فقط از حروف انگلیسی استفاده کنید');
            return;
        }
        var numFormat = /^\d+$/;
    if(!numFormat.test(document.getElementsByName('ID'+index)[0].value)) {
            alert('شماره ملی رابه عدد وارد کنید');
            return;
        }
        if(document.getElementsByName('ID'+index)[0].value.length != '10'){
            alert('شماره ملی اشتباه وارد شده است');
            return;
        }
    }
    document.getElementsByName('adultCount')[0].value = i;
    document.getElementsByName('childCount')[0].value = j - i;
    document.getElementsByName('infantCount')[0].value = k - j;
    document.getElementById('payForm').submit();
}
