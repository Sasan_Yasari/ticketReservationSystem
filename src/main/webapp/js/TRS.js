var app = angular.module('TRS', ['ngRoute']);
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "home.html"
    })
    .when("/login", {
        templateUrl : "login.html"
    })
    .when("/signup", {
        templateUrl : "SignUp.html"
    })
    .when("/search", {
        templateUrl : "search.html"
    })
    .when("/reserve", {
        templateUrl : "reserve.html"
    })
    .when("/ticket", {
        templateUrl : "ticket.html"
    });
})
app.controller("FlightController", function($http, $scope, $location){
    $scope.airline = "IR";
    localRef = this;
    this.flights = [];
    this.selectedFlight = [];
    this.passengers = [];
    this.tickets = [];
    this.username;
    this.getFlights = function(searchInfo) {
        $scope.adultCount = searchInfo.adultCount;
        $scope.childCount = searchInfo.childCount;
        $scope.infantCount = searchInfo.infantCount;
        $http.post('http://localhost:8080/AkbarTicket/AkbarTicket/search', searchInfo).then(
            function(response) {
                $scope.flights = response.data;
            });
    };

    this.setValue= function(key, seat, price) {
        return {
            key: key,
            seat: seat,
            price: price
        };
    };
    this.getFlight = function(key, seat, price) {
        $scope.key = key;
        $http.post('http://localhost:8080/AkbarTicket/AkbarTicket/usermanagement/user').then(
            function(response) {
                if(response.data === 'admin') {
                    window.location.href = 'http://localhost:8080/AkbarTicket/#/search';
                    alert("admin can not buy ticket!");
                }
                else if(response.data === 'user') {
                    window.location.href = 'http://localhost:8080/AkbarTicket/#/reserve';
                    $http.post('http://localhost:8080/AkbarTicket/AkbarTicket/reserve',
                        localRef.setValue(key, seat, price)).then(
                        function(response) {
                            $scope.selectedFlight = response.data;
                            if($scope.selectedFlight.totalPrice !== $scope.selectedFlight.prevPrice)
                                alert('قیمت جدید: ' + $scope.selectedFlight.totalPrice);
                        });
                } else
                    window.location.href='http://localhost:8080/AkbarTicket/#/login';
            });
    };
    this.sendPassengers = function(passengers) {
        $http.post('http://localhost:8080/AkbarTicket/AkbarTicket/ticket', passengers).then(
        function(response) {
            $scope.tickets = response.data;
        });
    };
    this.seatChange = function(seatName){
        $scope.seat = seatName;
        $scope.airline = "";
    };
    this.airline = function(airlineCode){
        $scope.seat = "";
        $scope.airline = airlineCode;
    };
    this.repS = function (flights, i) {
        for (var j=0; j<i; j++){
            if(flights[j].seatClass.name === flights[i].seatClass.name)
                return true;
        }
        return false;
    };

    this.repA = function (flights, i) {
        for (var j=0; j<i; j++){
            if(flights[j].airlineCode === flights[i].airlineCode)
                return true;
        }
        return false;
    };

    this.marshal = function(userInfo) {
        return {
            username: userInfo.username,
            password: userInfo.password,
            email: userInfo.email
        };
    };
    this.login = function(userInfo){
        var loginRef = this;
        $http.post('http://localhost:8080/AkbarTicket/AkbarTicket/usermanagement/login',
            this.marshal(loginRef.userInfo)).then(
            function(response) {
                if(response.data === 'user') {
                    localRef.username = loginRef.username;
                    window.location.href='http://localhost:8080/AkbarTicket/#/reserve';
                } else if(response.data === 'admin') {
                    localRef.username = loginRef.username;
                    window.location.href='http://localhost:8080/AkbarTicket/#/search';
                } else {
                    alert("نام کاربری یا گذرواژه نامعتبر است!");
                }
            }
        );
    };
    this.create = function(userInfo){
        var createRef = this;
        $http.post('http://localhost:8080/AkbarTicket/AkbarTicket/usermanagement/create',
            this.marshal(createRef.userInfo)).then(
            function(response) {
                if(response.data === true) {
                    window.location.href='http://localhost:8080/AkbarTicket/#/login';
                } else {
                    alert("از نام کاربری دیگری استفاده کنید!");
                }
            }
        );
    };

    this.logout = function() {
        $http.post('http://localhost:8080/AkbarTicket/AkbarTicket/usermanagement/logout');
        window.location.href='http://localhost:8080/AkbarTicket/';
    }
});

app.controller("validController", ['$scope', function($scope) {
    $scope.regex = '^[a-zA-Z0-9._-]+$';
    $scope.number = '^[0-9]+$';
}]);
app.filter('range', function() {
    return function(input, count) {
        count = parseInt(count);
        for (var i=0; i<count; i++) {
            input.push(i);
        }
        return input;
    };
});