//package common;
//
//import org.junit.Before;
//import org.junit.Test;
//
//import java.util.ArrayList;
//
//import static org.junit.Assert.*;
//
///**
// * Created by sasan on 2/13/17.
// */
//public class flightTest {
//    Flight f;
//    @Before
//    public void setUp() throws Exception {
//        ArrayList<Character> sc;
//        f = new Flight("IR 452 05Feb THR MHD 1740 1850 M80 Y7 FC BA");
//        sc = f.getSc();
//        f.setPrice(sc.get(0), 5, 3, 1);
//        f.setPrice(sc.get(1), 20, 16, 14);
//    }
//
//    @Test
//    public void getInfo() throws Exception {
//        assertTrue(f.getInfo(3, 2, 1).equals("Flight: IR 452 Departure: 17:40 " +
//                "Arrival: 18:50 Airplane: M80\nClass: Y Price: 22\nClass: B Price: 106\n"));
//    }
//
//}